#include "Record.h"
#include <format>

bool Record::operator > (const Record& record)
{
    return id > record.id;
}

bool Record::operator < (const Record& record)
{
    return id < record.id;
}

string Record::toString()
{
    return format(" {:<3} | {:<18} |     {:<10} |  {:<2} | {:<15} | {:<8} | {:<13} | {:<7}",
    id, fio, birthdayDate.toString(), gender, post, 
    experience, division, salary);
}
