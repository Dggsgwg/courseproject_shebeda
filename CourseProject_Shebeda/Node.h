#pragma once
#include "Record.h"

struct Node
{
    Record record;
    Node* next;
    
    string toString();
};