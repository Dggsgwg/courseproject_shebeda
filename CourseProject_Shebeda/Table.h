#pragma once
#include "Node.h"

class Table
{
    string name;
    Node* list;
	
public:
    Table();
    Table(string _tableName);
    Table(string _tableName, Node* _list);

    void display();
    void addRecord(Record record);
    Record getRecordFromStream(istream& in);
    bool removeRecord(int personalId);
    bool editRecord(int id, int fieldNum, string value);
    void sortAsc();
    void sortDesc();
    Node* search(int personalId);
    void saveToFile(string fileName);
    void readFromFile(string fileName);
    void printHeader();
    void getFiveMostExperienced();
    void getFiveHighestPaid();
    void clear();

private:
    bool displayPage(Node* showList, int rows, int page);
};

