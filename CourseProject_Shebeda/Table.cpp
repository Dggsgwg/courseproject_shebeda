#pragma once
#include <iostream>
#include <fstream>
#include <format>
#include <Windows.h>
#include <conio.h>
#include "Table.h"

Table::Table()
{
    name = "";
    list = nullptr;
}

Table::Table(string _tableName)
{
    name = _tableName;
    list = nullptr;
}

Table::Table(string _tableName, Node* _list)
{
    name = _tableName;
    list = _list;
}

istream& operator >>(istream& in, Record& r)
{
    string isMale, firstName, middleName, lastName;
    in >> r.id >> firstName >> middleName >> lastName
        >> r.birthdayDate.day >> r.birthdayDate.month
        >> r.birthdayDate.year >> r.gender >> r.post
        >> r.experience >> r.division >> r.salary;
    r.fio = format("{} {} {}", firstName, middleName, lastName);

    return in;
}

void Table::printHeader()
{
    cout << " Id  |        ���         |  ���� �������� | ��� |    ���������    |   ����   | ������������� | �����  " << endl;
}

bool Table::displayPage(Node* showList, int rows, int page)
{
    if (showList == NULL)
    {
        cout << "������� �����" << endl;
        cout << "������� ESC ��� ���������� ���������";
        return false;
    }

    for (int i = 0; i < rows * page; i++)
    {
        if (showList == NULL) return false;
        else showList = showList->next;
    }

    if (showList != NULL)
    {
        cout << "�������: " << name << ", �������� - " << page + 1 << endl;
        printHeader();
        while (showList)
        {
            if (rows <= 0) break;
            cout << showList->toString() << endl;
            showList = showList->next;
            rows--;
        }
        cout << "������� ESC ��� ���������� ���������";
        if (rows > 0 || showList == NULL) return false;
    }

    return true;
}

void Table::display()
{
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    GetConsoleScreenBufferInfo(hConsole, &csbi);
    int rows = csbi.srWindow.Bottom - csbi.srWindow.Top - 2;
    Node* showList = list;
    int currentPage = 0;
    int ch;
    bool exit = false;
    bool forward = true;

    system("cls");
    displayPage(showList, rows, 0);

    while (!exit)
    {
        ch = _getch();
        switch (ch)
        {
        case 224:
            {
                switch (_getch())
                {
                case 75:
                    {
                        // ������ ������� �����
                        if (currentPage > 0)
                        {
                            system("cls");
                            showList = list;
                            currentPage--;
                            forward = displayPage(showList, rows, currentPage);
                        }
                        break;
                    }
                case 77:
                    {
                        // ������ ������� ������
                        if (forward)
                        {
                            system("cls");
                            showList = list;
                            currentPage++;
                            forward = displayPage(showList, rows, currentPage);
                        }
                        break;
                    }
                default:
                    break;
                }
                break;
            }
        case 27:
            {
                exit = true;
                break;
            }
        default:
            break;
        }
    }

    showList = nullptr;
}

void Table::sortAsc() //���������� ���������
{
    Node* head = list;
    Node* min;

    for (Node* i = head; i != NULL; i = i->next)
    {
        min = i;

        for (Node* j = i->next; j; j = j->next)
        {
            if (j->record < min->record)
                min = j;
        }

        Record tmp = min->record;
        min->record = i->record;
        i->record = tmp;
    }

    head, min = nullptr;
}

void Table::sortDesc() //���������� ���������
{
    Node* head = list;
    Node* min;

    for (Node* i = head; i != NULL; i = i->next)
    {
        min = i;

        for (Node* j = i->next; j; j = j->next)
        {
            if (j->record > min->record)
                min = j;
        }

        Record tmp = min->record;
        min->record = i->record;
        i->record = tmp;
    }

    head, min = nullptr;
}

void Table::addRecord(Record record)
{
    Node* newElem = new Node{ record, list };
    list = newElem;
    
    newElem = nullptr;
}

Record Table::getRecordFromStream(istream& in)
{
    Record r;
    in >> r;

    return r;
}

bool Table::removeRecord(int id)
{
    Node* head = list;

    if (head != NULL)
    {
        if (list->record.id == id)
        {
            list = list->next;
            return true;
        }
        while (head->next) {
            if (head->next->record.id == id)
            {
                head->next = head->next->next;
                return true;
            }
            head = head->next;
        }
    }

    return false;
}

bool Table::editRecord(int id, int fieldNum, string value)
{
    Node* head = list;

    if (head != NULL)
    {
        while (head) {
            if (head->record.id == id)
            {
                Record r = head->record;
                switch (fieldNum) {
                case 0:
                    {
                        r.fio = value;
                        break;
                    }
                case 1:
                    {
                        int index = value.find_first_of(" ");
                        short day = stoi(value.substr(0, index));
                        value = value.substr(index + 1, value.size());
                        index = value.find_first_of(" ");
                        short month = stoi(value.substr(0, index));
                        value = value.substr(index + 1, value.size());
                        short year = stoi(value);
                        r.birthdayDate = Date{ year, month, day };
                        break;
                    }
                case 2:
                    {
                        r.gender = value;
                        break;
                    }
                case 3:
                    {
                        r.post = value;
                        break;
                    }
                case 4:
                    {
                        try {
                            r.experience = stoi(value);
                        }
                        catch (...) {}
                        break;
                    }
                case 5:
                    {
                        r.division = value;
                        break;
                    }
                case 6:
                    {
                        try {
                            r.salary = stoi(value);
                        }
                        catch (...) {}
                        break;
                    }
                }

                head->record = r;

                head = nullptr;
                return true;
            }

            head = head->next;
        }
    }

    head = nullptr;
    return false;
}

Node* Table::search(int id)
{
    Node* head = list;

    while (head)
    {
        if (head->record.id == id)
        {
            return head;
        }

        head = head->next;
    }

    return NULL;
}

void Table::getFiveMostExperienced()
{
    Node** array = new Node * [5];
    Node* head = list;
    int count = 0;

    while (head)
    {
        if (count < 5) {
            array[count] = head;
            count++;
        }
        else {
            int temp = 1000;
            int index = 0;
            for (int i = 0; i < 5; i++)
            {
                if (array[i]->record.experience < temp)
                {
                    temp = array[i]->record.experience;
                    index = i;
                }
            }
            if (head->record.experience > array[index]->record.experience)
            {
                array[index] = head;
            }
        }
        head = head->next;
    }

    printHeader();
    for (int i = 0; i < 5; i++)
    {
        cout << array[i]->toString() << endl;
    }

    head = nullptr;
}

void Table::getFiveHighestPaid() 
{
    Node** array = new Node * [5];
    Node* head = list;
    int count = 0;

    while (head)
    {
        if (count < 5) {
            array[count] = head;
            count++;
        }
        else {
            double temp = 1000;
            int index = 0;
            for (int i = 0; i < 5; i++)
            {
                if (array[i]->record.salary < temp)
                {
                    temp = array[i]->record.salary;
                    index = i;
                }
            }
            if (head->record.salary > array[index]->record.salary)
            {
                array[index] = head;
            }
        }
        head = head->next;
    }

    printHeader();
    for (int i = 0; i < 5; i++)
    {
        cout << array[i]->toString() << endl;
    }

    head = nullptr;
}

void Table::saveToFile(string fileName)
{
    ofstream out;
    out.open(format("{}.table", fileName));
    Node* head = list;

    if (out.is_open())
    {
        while (head)
        {
            out << head->toString() << endl;
            head = head->next;
        }
    }

    head = nullptr;
}

void Table::readFromFile(string fileName)
{
    name = fileName;
    ifstream in(format("{}.table", fileName));
    Record r = Record();

    if (in.is_open())
    {
        while (in >> r)
        {
            addRecord(r);
        }
    }
    sortAsc();
}

void Table::clear()
{
    list = NULL;
}
