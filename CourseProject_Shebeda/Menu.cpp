﻿#include <iostream>
#include <conio.h>
#include <Windows.h>
#include "Menu.h"

using namespace std;

void Menu::printMenu(int num, int position)
{
	const int size = menuSizes[num];
	const string* first = new string[]{
		"Создание новой таблицы",
		"Загрузка таблицы из файла",
		"Продолжить работу с таблицей",
		"Сохранение текущей таблицы в файл",
		"Выход из программы"
	};
	const string* second = new string[]{
		"Выход из программы",
		"Продолжить работу с таблицей"
	};
	const string* third = new string[]{
		"Вернуться в главное меню",
		"Просмотр таблицы",
		"Добавить запись",
		"Удалить запись",
		"Изменить запись",
		"Сортировать таблицу",
		"Найти запись",
		"Получить 5 самых опытных клерков",
		"Получить 5 клерков с наибольшим окладом"
	};
	const string* fourth = new string[]{
		"Сортировка по возрастанию",
		"Сортировка по убыванию"
	};
	const string* fifth = new string[]{
		"ФИО",
		"Дата рождения",
		"Пол (М/Ж)",
		"Должность",
		"Стаж",
		"Подразделение",
		"Оклад"
	};

	switch (num)
	{
	case 0:
		cout << "Список команд:" << endl;
		for (int i = 0; i < size; i++) {
			if (i == position) {
				cout << "[ " << first[i] << " ]" << endl;
			}
			else {
				cout << first[i] << endl;
			}
		}
		break;
	case 1:
		cout << "Список команд:" << endl;
		for (int i = 0; i < size; i++) {
			if (i == position) {
				cout << "[ " << second[i] << " ]" << endl;
			}
			else {
				cout << second[i] << endl;
			}
		}
		break;
	case 2:
		cout << "Список команд:" << endl;
		for (int i = 0; i < size; i++) {
			if (i == position) {
				cout << "[ " << third[i] << " ]" << endl;
			}
			else {
				cout << third[i] << endl;
			}
		}
		break;
	case 3:
		cout << "Список команд:" << endl;
		for (int i = 0; i < size; i++) {
			if (i == position) {
				cout << "[ " << fourth[i] << " ]" << endl;
			}
			else {
				cout << fourth[i] << endl;
			}
		}
		break;
	case 4:
		cout << "Выберите поле для изменения:" << endl;
		for (int i = 0; i < size; i++) {
			if (i == position) {
				cout << "[ " << fifth[i] << " ]" << endl;
			}
			else {
				cout << fifth[i] << endl;
			}
		}
		break;
	}

}

int Menu::getMenuPosition(int menuId, int menuSize, string defaultText) {
	int position = 0;
	int ch;
	bool exit = false;
	INPUT_RECORD InputRecord;
	system("cls");
	if (defaultText != "") {
		t.printHeader();
		cout << defaultText << endl;
	}
	printMenu(menuId, position);

	while (!exit)
	{
		ch = _getch();
		switch (ch)
		{
		case 224:
		{
			switch (_getch())
			{
			case 72:
			{// нажата клавиша вверх
				if (position > 0) {
					position -= 1;
					system("cls");
					if (defaultText != "") {
						t.printHeader();
						cout << defaultText << endl;
					}
					printMenu(menuId, position);
				}
				break;
			}
			case 80:
			{// нажата клавиша вниз
				if (position < menuSize - 1) {
					position += 1;
					system("cls");
					if (defaultText != "") {
						t.printHeader();
						cout << defaultText << endl;
					}
					printMenu(menuId, position);
				}
				break;
			}
			default:
				break;
			}
			break;
		}
		case 13:
		{
			exit = true;
			break;
		}
		default:
			break;
		}
	}

	return position;
}

void Menu::operateWithTable()
{
	int command, numLine, pos;
	string line;
	Node* n = NULL;
	bool skip = false;
	int isAsc = -1;

	do
	{
		system("cls");

		command = getMenuPosition(2, menuSizes[2], "");
		system("cls");

		switch (command)
		{
		case 1:
			t.display();
			skip = true;
			break;
		case 2:
			cout << "Введите данные записи в формате:" << endl
				<< "id Ф И О день месяц год пол должность стаж подразделение оклад:" << endl
				<< "1 Иванов И И 1 1 2001 М Кассир 20 Бухгалтерия 25000" << endl;
			t.addRecord(t.getRecordFromStream(cin));
			t.display();
			skip = true;
			break;
		case 3:
			cout << "Введите id для удаления записи:" << endl;
			cin >> numLine;
			t.removeRecord(numLine);
			t.display();
			skip = true;
			break;
		case 4:
			cout << "Введите id для изменения записи:" << endl;
			cin >> numLine;
			system("cls");
			n = t.search(numLine);
			if (n != NULL)
			{
				line = n->toString();
			}
			else
			{
				cout << "Запись не найдена" << endl;
				break;
			}
			pos = getMenuPosition(4, menuSizes[4], line);
			system("cls");

			cout << "Введите новое значение поля:" << endl;
			cin.seekg(cin.eof());
			getline(cin, line);

			t.editRecord(numLine, pos, line);
			t.display();
			skip = true;
			break;
		case 5:
			isAsc = getMenuPosition(3, menuSizes[3], "");
			if (isAsc == 0)
			{
				t.sortAsc();
			}
			else {
				t.sortDesc();
			}
			t.display();
			skip = true;
			break;
		case 6:
			cout << "Введите id для поиска записи:" << endl;
			cin >> numLine;
			n = t.search(numLine);
			if (n != NULL)
			{
				cout << "Искомая запись:" << endl;
				t.printHeader();
				cout << n->toString() << endl;
			}
			else
			{
				cout << "Запись не найдена" << endl;
			}
			break;
		case 7:
			t.getFiveMostExperienced();
			break;
		case 8:
			t.getFiveHighestPaid();
			break;
		}
		
		if (command != 0)
		{
			if (!skip)
			{
				system("pause");
			}
			else {
				skip = false;
			}
		}
	} while (command != 0);

	n = nullptr;
}

void Menu::saveToFile(bool leave)
{
	string name;
	int command;

	cout << "Введите имя файла для сохранения" << endl;
	cin >> name;
	
	t.saveToFile(name);

	if (!leave) {
		do
		{
			system("cls");
			command = getMenuPosition(1, menuSizes[1], "");

			switch (command)
			{
			case 1:
				operateWithTable();
				break;
			}
		} while (command != 0);
	}
	exit(0);
}

void Menu::loadFromFile()
{
	t.clear();
	string fileName;

	cout << "Введите имя файла для открытия" << endl;
	cin >> fileName;

	t.readFromFile(fileName);
	operateWithTable();
}

void Menu::start()
{
	int command;
	string name;
	string fileName;

	do
	{
		system("cls");
		command = getMenuPosition(0, menuSizes[0], "");
		system("cls");

		switch (command)
		{
		case 0:
			cout << "Введите название таблицы:" << endl;
			cin >> name;

			t = Table(name);
			operateWithTable();
			break;
		case 1:
			loadFromFile();
			break;
		case 2:
			operateWithTable();
			break;	
		case 3:
			cout << "Введите имя файла для сохранения" << endl;
			cin >> fileName;

			t.saveToFile(fileName);

			system("cls");
			command = getMenuPosition(1, menuSizes[1], "");

			switch (command)
			{
			case 1:
				operateWithTable();
				break;
			case 0:
				exit(0);
			}
			
			break;
		case 4:
			while (command != -1) {
				cout << "Сохранить таблицу в файл? (y/n)" << endl;
				char ch = _getch();
				cout << ch << endl;
				switch (ch)
				{
				case 'y':
					saveToFile(true);
					command = -1;
					break;
				case 'n':
					command = -1;
					break;
				default:
					break;
				}
			}
			break;
		}
	} while (command != -1);
}