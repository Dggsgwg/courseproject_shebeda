﻿#include "Table.h"

class Menu
{
    Table t = Table();
    
public:
    void start();
    
private:
    const int* menuSizes = new int[] { 5, 2, 9, 2, 7 };

    int getMenuPosition(int menuId, int menuSize, string defaultText);
    void loadFromFile();
    void saveToFile(bool leave);
    void operateWithTable();
    void printMenu(int num, int position);
};
