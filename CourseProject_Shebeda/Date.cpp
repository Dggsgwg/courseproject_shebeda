#include "Date.h"
#include <format>

int Date::compare(Date date) 
{
    if (year > date.year) return  1;
    else if (year < date.year) return -1;
    if (month > date.month) return 1;
    else if (month < date.month) return -1;
    if (day > date.day) return 1;
    else if (day < date.day) return -1;
    else return 0;
}

string Date::toString() 
{
    return format("{} {} {}", day, month, year);
}