#pragma once
#include <string>

using namespace std;

struct Date
{
    short year;
    short month;
    short day;

    int compare(Date date);
    string toString();
};
